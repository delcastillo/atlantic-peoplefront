import { Routes } from "@angular/router";
import { AdminLayoutComponent } from "./shared/components/layouts/admin-layout/admin-layout.component";
import { AuthLayoutComponent } from "./shared/components/layouts/auth-layout/auth-layout.component";
import { AuthGuard } from "./shared/services/auth/auth.guard";

export const rootRouterConfig: Routes = [
  {
    path: "",
    redirectTo: "form",
    pathMatch: "full"
  },
  {
    path: "",
    component: AuthLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "admin",
        loadChildren: () =>
          import("./views/admin/admin.module").then(m => m.AdminModule),
          data: { title: "Admin", breadcrumb: "ADMIN" }
      }
    ]
  },
  {
    path: "",
    component: AuthLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "form",
        loadChildren: () =>
          import("./views/form/form.module").then(m => m.FormModule),
          data: { title: "Form", breadcrumb: "FORM" }
      }
    ]
  },
  {
    path: "**",
    redirectTo: "sessions/404"
  }
];
