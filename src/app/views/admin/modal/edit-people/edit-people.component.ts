import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ICommonResponse } from 'app/shared/interfaces/icommon-response';
import { IPeople } from 'app/shared/interfaces/ipeople';
import { RestApiService } from 'app/shared/services/rest-api/rest-api.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-edit-people',
  templateUrl: './edit-people.component.html',
  styleUrls: ['./edit-people.component.css']
})
export class EditPeopleComponent implements OnInit {

  EditPerson: FormGroup;
  constructor(
    public  dialogRef:  MatDialogRef<EditPeopleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IPeople,
    private apiService: RestApiService,
    private snotifyService: SnotifyService,
  ) { 

    this.EditPerson = new FormGroup({
      ID:new FormControl(data.ID),
      IdentificationNumber: new FormControl(data.IdentificationNumber,Validators.required),
      FirstName: new FormControl(data.FirstName,Validators.required),
      LastName: new FormControl(data.LastName,Validators.required),
      FirstSurName: new FormControl(data.FirstSurName,Validators.required),
      LastSurName: new FormControl(data.LastSurName,Validators.required),
      NumberContact: new FormControl(data.NumberContact,Validators.required),
      Email: new FormControl(data.Email,Validators.required),
      Bitth: new FormControl(data.Bitth,Validators.required),
      Sure: new FormControl(data.Sure,Validators.required),
      
    });
  }

  ngOnInit(): void {
  }

  editPerson(){
    this.apiService
    .Post('People/Save',this.EditPerson.value)
    .subscribe((response: ICommonResponse)=>{
      if (response.Status == 'Success') {
        this.EditPerson.reset();
        this.snotifyService.success('Record updated successfully');
        this.dialogRef.close(true);
      }      
    },(error: HttpErrorResponse)=>{
      let info = <ICommonResponse>error.error;
      console.error('ERROR', info);
    })
  }

}
