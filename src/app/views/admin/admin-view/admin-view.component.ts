import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ICommonResponse } from 'app/shared/interfaces/icommon-response';
import { IPeople } from 'app/shared/interfaces/ipeople';
import { RestApiService } from 'app/shared/services/rest-api/rest-api.service';
import { SnotifyService } from 'ng-snotify';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { EditPeopleComponent } from "../modal/edit-people/edit-people.component";

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.css']
})
export class AdminViewComponent implements OnInit {

  filterPoeple: string = '';
  people: IPeople[];
  PeobleArray: Array<IPeople> = [];
  constructor(
    private apiService: RestApiService,
    private snotifyService: SnotifyService,
    public  dialog: MatDialog
  ) { }

  ngOnInit(): void {  
    this.getPeople()
  }

  
    deleteUser(ID: number): void {
      this.snotifyService.confirm(
        'record will be removed',
        'Are you sure?',
        {
          closeOnClick: true,
          pauseOnHover: true,
          buttons: [
            {
              text: 'No',
              action: () => {
                this.snotifyService.warning('Cancelled')
              },
            },
            {
              text: 'Yes',
              action: () => {
                this.apiService
                  .Get('People/Delete/' + ID)
                  .subscribe((Response: ICommonResponse) => {
                    if (Response.Status == 'Success') {
                      this.snotifyService.success('the record has been deleted')
                      this.getPeople()
                    } else {
                      this.snotifyService.error('An error has occurred')
                    }
                  })
              },
            },
          ],
        }
      )
    }

    getPeople(){
      this.apiService
      .Get('People/getPeople')
      .subscribe((response: ICommonResponse)=>{
        this.people = <IPeople[]>response.Data
        this.SearchPerson();       
      },
      (error: HttpErrorResponse) =>{
        console.log("Error");
        console.log(<ICommonResponse>error.error);
      });
    }

    UpdatePerson(person: IPeople){
      const dialogRef = this.dialog.open(EditPeopleComponent,{
        width: '800px',
        data: person
      });
      dialogRef.afterClosed().subscribe((result: boolean) =>{
        if (result) {
          this.getPeople();
        }
      })
    }

    SearchPerson(){
      const val = this.filterPoeple.toUpperCase().trim();
      if (val.length <= 0) {
        this.PeobleArray = [...this.people];
        return;
      }
      var columns = ['IdentificationNumber'];
      this.PeobleArray = this.people.filter((d) =>{
        for (let i = 0; i < columns.length; i++) {
          let column = columns[i];
          if(d[column] && d[column].toString().toUpperCase().indexOf(val) > -1) return true;
        }
      })
    }
}
