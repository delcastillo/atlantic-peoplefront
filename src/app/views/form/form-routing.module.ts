import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormViewComponent } from "./form-view/form-view.component";


const routes: Routes = [
  {
    path: '',
    children: [{
      path: '',
      component: FormViewComponent,
      data: { title: 'Form', breadcrumb: 'FORM' }
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormRoutingModule { }
