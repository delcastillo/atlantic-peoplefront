import { formatCurrency, getCurrencySymbol } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, NgForm, FormGroupDirective, FormControl, AbstractControl, ValidationErrors } from "@angular/forms";


import { FormBuilder } from "@angular/forms";
import { MatSnackBar } from '@angular/material/snack-bar';
import { ICommonResponse } from 'app/shared/interfaces/icommon-response';
import { RestApiService } from 'app/shared/services/rest-api/rest-api.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-form-view',
  templateUrl: './form-view.component.html',
  styleUrls: ['./form-view.component.css']
})
export class FormViewComponent implements OnInit {


  PersonForm: FormGroup;
  constructor(
    private apiService: RestApiService,
    private snotifyService: SnotifyService,
  ) {

    this.PersonForm = new FormGroup({
      IdentificationNumber: new FormControl(null,Validators.required),
      FirstName: new FormControl('',Validators.required),
      LastName: new FormControl('',Validators.required),
      FirstSurName: new FormControl('',Validators.required),
      LastSurName: new FormControl('',Validators.required),
      NumberContact: new FormControl(null,Validators.required),
      Email: new FormControl('',Validators.required),
      Bitth: new FormControl('',Validators.required),
      Sure: new FormControl(null,Validators.required),
      
    });
   }

  ngOnInit(): void {

  }

  onSubmit() {
    this.apiService
    .Post('People/Save',this.PersonForm.value)
    .subscribe((response: ICommonResponse)=>{
      if (response.Status == 'Success') {
        this.PersonForm.reset();
        // this.snotifyService.success('Record saved successfully');
        console.log("Record saved successfully");
        
      }      
    },(error: HttpErrorResponse)=>{
      let info = <ICommonResponse>error.error;
      console.error('ERROR', info);
    })
  }
}
